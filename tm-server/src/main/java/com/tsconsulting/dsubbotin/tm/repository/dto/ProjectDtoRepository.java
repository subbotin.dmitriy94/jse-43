package com.tsconsulting.dsubbotin.tm.repository.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.IProjectDtoRepository;
import com.tsconsulting.dsubbotin.tm.dto.ProjectDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectDtoRepository extends AbstractOwnerDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager
                .createQuery("FROM ProjectDTO", ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM ProjectDTO WHERE userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public ProjectDTO findById(@NotNull final String id) throws AbstractException {

        return entityManager
                .createQuery("FROM ProjectDTO WHERE id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);

    }

    @NotNull
    @Override
    public ProjectDTO findById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM ProjectDTO WHERE userId = :userId AND id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public ProjectDTO findByIndex(final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM ProjectDTO", ProjectDTO.class)
                .setParameter("index", index)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public ProjectDTO findByIndex(@NotNull final String userId, final int index) throws AbstractException {

        return entityManager
                .createQuery("FROM ProjectDTO WHERE userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);

    }

    @Override
    public @NotNull ProjectDTO findByName(@NotNull final String userId, final @NotNull String name) throws AbstractException {
        return entityManager
                .createQuery("FROM ProjectDTO WHERE userId = :userId AND name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);

    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE ProjectDTO WHERE userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("FROM ProjectDTO WHERE userId = :userId AND id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .isPresent();
    }

}
