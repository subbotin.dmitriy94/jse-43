package com.tsconsulting.dsubbotin.tm.service.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.ISessionDtoRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.dto.IUserDtoRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.dto.ISessionDtoService;
import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.repository.dto.SessionDtoRepository;
import com.tsconsulting.dsubbotin.tm.repository.dto.UserDtoRepository;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class SessionDtoService extends AbstractDtoService<SessionDTO> implements ISessionDtoService {

    public SessionDtoService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    @Override
    public SessionDTO open(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @Nullable final UserDTO user = userRepository.findByLogin(login);
            if (sessionRepository.existByUserId(user.getId())) throw new AccessDeniedException();
            @NotNull final SessionDTO session = new SessionDTO();
            @NotNull final IPropertyService propertyService = new PropertyService();
            final int iteration = propertyService.getPasswordIteration();
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final String hash = HashUtil.salt(iteration, secret, password);
            if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
            session.setUserId(user.getId());
            session.setDate(new Date());
            session.setSignature(null);
            entityManager.getTransaction().begin();
            sessionRepository.open(sign(session));
            entityManager.getTransaction().commit();
            return session;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean close(@NotNull final SessionDTO session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            if (!sessionRepository.contains(session)) return false;
            entityManager.clear();
            entityManager.getTransaction().begin();
            sessionRepository.close(session);
            entityManager.getTransaction().commit();
            return !sessionRepository.contains(session);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void validate(
            @Nullable final SessionDTO session,
            @NotNull final Role role
    ) throws AbstractException {
        validate(session);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            if (session == null) throw new AccessDeniedException();
            @NotNull final String userId = session.getUserId();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = userRepository.findById(userId);
            @NotNull final Role userRole = user.getRole();
            if (!userRole.equals(role)) throw new AccessDeniedException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void validate(@Nullable final SessionDTO session) throws AbstractException {
        checkSession(session);
        @Nullable final SessionDTO sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        if (signatureSource == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sign(sessionTemp).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            if (!sessionRepository.contains(session)) throw new AccessDeniedException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO getUser(@NotNull final SessionDTO session) throws AbstractException {
        @NotNull final String userId = session.getUserId();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @Nullable final UserDTO user = userRepository.findById(userId);
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final SessionDTO session) {
        return session.getUserId();
    }

    @NotNull
    private SessionDTO sign(@NotNull final SessionDTO session) {
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = new PropertyService();
        final int iteration = propertyService.getSignatureIteration();
        @NotNull String secret = propertyService.getSignatureSecret();
        @Nullable final String signature = HashUtil.sign(iteration, secret, session);
        session.setSignature(signature);
        return session;
    }

    private void checkSession(@Nullable final SessionDTO session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getDate().toString())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getSignature())) throw new AccessDeniedException();
    }

    @Override
    public @NotNull List<SessionDTO> findAll() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            return sessionRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull SessionDTO findById(@NotNull final String id) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            return sessionRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull SessionDTO findByIndex(final int index) throws AbstractException {
        final int realIndex = index - 1;
        if (realIndex < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            return sessionRepository.findByIndex(realIndex);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            sessionRepository.clear();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final List<SessionDTO> sessions) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull final SessionDTO session : sessions) sessionRepository.create(session);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

}
