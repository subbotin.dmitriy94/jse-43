package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    void setPasswordUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "password") String password
    ) throws AbstractException;

    @WebMethod
    void updateByIdUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "lastName") String lastName,
            @NotNull @WebParam(name = "firstName") String firstName,
            @NotNull @WebParam(name = "middleName") String middleName,
            @NotNull @WebParam(name = "email") String email
    ) throws AbstractException;

    @WebMethod
    boolean isLoginUser(
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

    @WebMethod
    UserDTO getUser(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws AbstractException;

}
