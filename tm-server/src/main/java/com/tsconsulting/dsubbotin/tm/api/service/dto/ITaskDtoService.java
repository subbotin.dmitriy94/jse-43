package com.tsconsulting.dsubbotin.tm.api.service.dto;

import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public interface ITaskDtoService extends IOwnerDtoService<TaskDTO> {

    @NotNull
    TaskDTO create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException;

    void removeById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void removeByIndex(@NotNull String userId, int index) throws AbstractException;

    void removeByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void updateById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException;

    void updateByIndex(
            @NotNull String userId,
            int index,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException;

    void startById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void startByIndex(@NotNull String userId, int index) throws AbstractException;

    void startByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void finishById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void finishByIndex(@NotNull String userId, int index) throws AbstractException;

    void finishByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void updateStatusById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull Status status
    ) throws AbstractException;

    void updateStatusByIndex(
            @NotNull String userId,
            int index,
            @NotNull Status status
    ) throws AbstractException;

    void updateStatusByName(
            @NotNull String userId,
            @NotNull String name,
            @NotNull Status status
    ) throws AbstractException;

}
