package com.tsconsulting.dsubbotin.tm.api.repository.dto;

import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskDtoRepository extends IOwnerDtoRepository<TaskDTO> {

    boolean existById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String id);

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String id);

}
