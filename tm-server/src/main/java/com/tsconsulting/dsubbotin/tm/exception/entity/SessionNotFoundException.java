package com.tsconsulting.dsubbotin.tm.exception.entity;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class SessionNotFoundException extends AbstractException {

    public SessionNotFoundException() {
        super("Session not found!");
    }

}