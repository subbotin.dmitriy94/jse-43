package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.dto.ProjectDTO;
import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.entity.Project;
import com.tsconsulting.dsubbotin.tm.entity.Session;
import com.tsconsulting.dsubbotin.tm.entity.Task;
import com.tsconsulting.dsubbotin.tm.entity.User;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory sessionFactory;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.sessionFactory = getSessionFactory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return this.sessionFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory getSessionFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, propertyService.getJdbcDriver());
        settings.put(URL, propertyService.getJdbcUrl());
        settings.put(USER, propertyService.getJdbcLogin());
        settings.put(PASS, propertyService.getJdbcPassword());
        settings.put(DIALECT, propertyService.getHibernateDialect());
        settings.put(SHOW_SQL, propertyService.getHibernateShowSql());
        settings.put(FORMAT_SQL, propertyService.getHibernateFormatSql());
        settings.put(HBM2DDL_AUTO, propertyService.getHibernateHbm2Ddl());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry serviceRegistry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(serviceRegistry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.buildMetadata();
        return metadata.getSessionFactoryBuilder().build();
    }

}
