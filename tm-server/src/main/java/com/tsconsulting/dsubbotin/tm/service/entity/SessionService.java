package com.tsconsulting.dsubbotin.tm.service.entity;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.entity.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.ISessionService;
import com.tsconsulting.dsubbotin.tm.entity.Session;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.repository.entity.SessionRepository;
import com.tsconsulting.dsubbotin.tm.repository.entity.UserRepository;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    @Override
    public Session open(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findByLogin(login);
            if (sessionRepository.existByUserId(user.getId())) throw new AccessDeniedException();
            @NotNull final Session session = new Session();
            @NotNull final IPropertyService propertyService = new PropertyService();
            final int iteration = propertyService.getPasswordIteration();
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final String hash = HashUtil.salt(iteration, secret, password);
            if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
            session.setUser(user);
            session.setDate(new Date());
            session.setSignature(null);
            entityManager.getTransaction().begin();
            sessionRepository.open(sign(session));
            entityManager.getTransaction().commit();
            return session;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean close(@NotNull final Session session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            if (!sessionRepository.contains(session)) return false;
            entityManager.clear();
            entityManager.getTransaction().begin();
            sessionRepository.remove(session);
            entityManager.getTransaction().commit();
            return !sessionRepository.contains(session);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void validate(
            @Nullable final Session session,
            @NotNull final Role role
    ) throws AbstractException {
        validate(session);
        if (session == null) throw new AccessDeniedException();
        @NotNull final Role userRole = session.getUser().getRole();
        if (!userRole.equals(role)) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session) throws AbstractException {
        checkSession(session);
        @Nullable final Session sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        if (signatureSource == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sign(sessionTemp).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            if (!sessionRepository.contains(session)) throw new AccessDeniedException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User getUser(@NotNull final Session session) throws AbstractException {
        @NotNull final String userId = session.getUser().getId();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findById(userId);
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        return session.getUser().getId();
    }

    @NotNull
    private Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = new PropertyService();
        final int iteration = propertyService.getSignatureIteration();
        @NotNull String secret = propertyService.getSignatureSecret();
        @Nullable final String signature = HashUtil.sign(iteration, secret, session);
        session.setSignature(signature);
        return session;
    }

    private void checkSession(@Nullable final Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getUser().getId())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getDate().toString())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getSignature())) throw new AccessDeniedException();
    }

    @NotNull
    @Override
    public List<Session> findAll() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            return sessionRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session findById(@NotNull final String id) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            return sessionRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session findByIndex(final int index) throws AbstractException {
        final int realIndex = index - 1;
        if (realIndex < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            return sessionRepository.findByIndex(realIndex);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            sessionRepository.clear();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final List<Session> sessions) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull final Session session : sessions) sessionRepository.create(session);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

}
