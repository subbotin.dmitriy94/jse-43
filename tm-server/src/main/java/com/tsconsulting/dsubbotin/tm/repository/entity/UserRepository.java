package com.tsconsulting.dsubbotin.tm.repository.entity;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.IUserRepository;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("FROM User", User.class).getResultList();
    }

    @NotNull
    @Override
    public User findById(@NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM User WHERE id = :id", User.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public User findByIndex(final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM User", User.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) throws AbstractException {
        return entityManager
                .createQuery("FROM User WHERE login = :login", User.class)
                .setParameter("login", login)
                .getResultStream()
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE User").executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM User where id <> :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existById(@NotNull String id) {
        return entityManager
                .createQuery("FROM User WHERE id = :id", User.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .isPresent();
    }

}
