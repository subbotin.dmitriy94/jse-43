package com.tsconsulting.dsubbotin.tm.service.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.IProjectDtoRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.dto.IProjectDtoService;
import com.tsconsulting.dsubbotin.tm.dto.ProjectDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.repository.dto.ProjectDtoRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class ProjectDtoService extends AbstractOwnerDtoService<ProjectDTO> implements IProjectDtoService {

    public ProjectDtoService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final List<ProjectDTO> projects = projectRepository.findAll();
            if (projects.isEmpty()) throw new ProjectNotFoundException();
            return projects;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<ProjectDTO> findAll(@NotNull final String userId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final List<ProjectDTO> projects = projectRepository.findAll(userId);
            if (projects.isEmpty()) throw new ProjectNotFoundException();
            return projects;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final String sort) throws AbstractException {
        @NotNull List<ProjectDTO> projects = findAll(userId);
        try {
            @NotNull final Sort sortType = EnumerationUtil.parseSort(sort);
            projects.sort(sortType.getComparator());
            return projects;
        } catch (UnknownSortException e) {
            return projects;
        }
    }

    @NotNull
    @Override
    public ProjectDTO findById(@NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findByIndex(final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findByIndex(realIndex);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findByIndex(@NotNull final String userId, final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findByIndex(userId, realIndex);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description) throws AbstractException {
        checkName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName(name);
            project.setDescription(description);
            project.setUserId(userId);
            entityManager.getTransaction().begin();
            projectRepository.create(userId, project);
            entityManager.getTransaction().commit();
            return projectRepository.findById(project.getId());
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final List<ProjectDTO> projects) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull ProjectDTO project : projects) projectRepository.create(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = findById(userId, id);
            project.setName(name);
            project.setDescription(description);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = projectRepository.findByIndex(userId, realIndex);
            project.setName(name);
            project.setDescription(description);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = findById(userId, id);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void startByIndex(@NotNull final String userId, final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = projectRepository.findByIndex(userId, realIndex);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkId(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = findByName(userId, name);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = findById(userId, id);
            project.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByIndex(@NotNull final String userId, final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = projectRepository.findByIndex(userId, realIndex);
            project.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = findByName(userId, name);
            project.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = findById(userId, id);
            project.setStatus(status);
            if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = projectRepository.findByIndex(userId, realIndex);
            project.setStatus(status);
            if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        checkName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = findByName(userId, name);
            project.setStatus(status);
            if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

    private void checkName(@NotNull final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

}
