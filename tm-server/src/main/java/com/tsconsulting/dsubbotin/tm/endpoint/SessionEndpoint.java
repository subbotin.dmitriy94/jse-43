package com.tsconsulting.dsubbotin.tm.endpoint;

import com.tsconsulting.dsubbotin.tm.api.endpoint.ISessionEndpoint;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public SessionDTO openSession(
            @NotNull @WebParam(name = "login") final String login,
            @NotNull @WebParam(name = "password") final String password
    ) throws AbstractException {
        return serviceLocator.getSessionService().open(login, password);
    }

    @Override
    @WebMethod
    public boolean closeSession(
            @NotNull @WebParam(name = "session") SessionDTO session
    ) throws AbstractException {
        return serviceLocator.getSessionService().close(session);
    }

    @NotNull
    @Override
    @WebMethod
    public SessionDTO register(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password,
            @NotNull @WebParam(name = "email") String email
    ) throws AbstractException {
        serviceLocator.getUserService().create(login, password, Role.USER, email);
        return openSession(login, password);
    }

}
