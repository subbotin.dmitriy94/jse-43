package com.tsconsulting.dsubbotin.tm.service.entity;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.IUserService;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyLoginException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyPasswordException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.repository.entity.UserRepository;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear(userId);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<User> findAll() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public User findById(@NotNull final String id) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public User findByIndex(final int index) throws AbstractException {
        final int realIndex = index - 1;
        if (realIndex < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findByIndex(realIndex);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findById(id);
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findByLogin(login);
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(getPasswordHash(password));
            entityManager.getTransaction().begin();
            userRepository.create(user);
            entityManager.getTransaction().commit();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        checkPassword(password);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(getPasswordHash(password));
            user.setRole(role);
            entityManager.getTransaction().begin();
            userRepository.create(user);
            entityManager.getTransaction().commit();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role,
            @NotNull final String email
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        checkPassword(password);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(getPasswordHash(password));
            user.setRole(role);
            user.setEmail(email);
            entityManager.getTransaction().begin();
            userRepository.create(user);
            entityManager.getTransaction().commit();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final List<User> users) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            for (User user : users) userRepository.create(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setPassword(
            @NotNull final String id,
            @NotNull final String password
    ) throws AbstractException {
        checkId(id);
        checkPassword(password);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final String passwordHash = getPasswordHash(password);
            @NotNull final User user = userRepository.findById(id);
            user.setPasswordHash(passwordHash);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setRole(
            @NotNull final String id,
            @NotNull final Role role
    ) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findById(id);
            user.setRole(role);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @NotNull final String id,
            @NotNull final String lastName,
            @NotNull final String firstName,
            @NotNull final String middleName,
            @NotNull final String email
    ) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findById(id);
            user.setLastName(lastName);
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setEmail(email);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            userRepository.findByLogin(login);
            return true;
        } catch (AbstractException e) {
            return false;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findByLogin(login);
            user.setLocked(true);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findByLogin(login);
            user.setLocked(false);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    private void checkLogin(@NotNull final String login) throws EmptyLoginException {
        if (EmptyUtil.isEmpty(login)) throw new EmptyLoginException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkPassword(@NotNull final String password) throws EmptyPasswordException {
        if (EmptyUtil.isEmpty(password)) throw new EmptyPasswordException();
    }

    @NotNull
    private String getPasswordHash(@NotNull final String password) {
        @NotNull final IPropertyService propertyService = new PropertyService();
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        return HashUtil.salt(iteration, secret, password);
    }

}