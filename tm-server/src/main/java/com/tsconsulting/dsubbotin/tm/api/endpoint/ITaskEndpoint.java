package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @Nullable @WebParam(name = "sort") String sort
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDTO findByIdTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDTO findByIndexTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void removeByIdTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void removeByIndexTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDTO createTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException;

    @WebMethod
    void removeAllTask(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDTO findByNameTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void removeByNameTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void updateByIdTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException;

    @WebMethod
    void updateByIndexTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException;

    @WebMethod
    void startByIdTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void startByIndexTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void startByNameTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void finishByIdTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void finishByIndexTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void finishByNameTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void updateStatusByIdTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException;

    @WebMethod
    void updateStatusByIndexTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException;

    @WebMethod
    void updateStatusByNameTask(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException;

}
