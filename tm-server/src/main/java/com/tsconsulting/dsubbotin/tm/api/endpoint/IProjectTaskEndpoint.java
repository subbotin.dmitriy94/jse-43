package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectTaskEndpoint {

    @WebMethod
    void bindTaskToProject(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId") String taskId
    ) throws AbstractException;

    @WebMethod
    void unbindTaskFromProject(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId") String taskId
    ) throws AbstractException;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasksByProjectId(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void removeProjectById(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void removeProjectByIndex(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void removeProjectByName(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void removeAllProject(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws AbstractException;

}
