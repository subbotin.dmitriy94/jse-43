package com.tsconsulting.dsubbotin.tm.api.repository.dto;

import com.tsconsulting.dsubbotin.tm.dto.ProjectDTO;
import org.jetbrains.annotations.NotNull;

public interface IProjectDtoRepository extends IOwnerDtoRepository<ProjectDTO> {

    boolean existById(@NotNull String userId, @NotNull String id);

}
