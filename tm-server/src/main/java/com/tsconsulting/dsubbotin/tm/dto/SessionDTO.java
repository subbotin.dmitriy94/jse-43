package com.tsconsulting.dsubbotin.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sessions")
public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Column
    @NotNull
    private Date date;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @Nullable
    @Column
    private String signature;

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }


}
