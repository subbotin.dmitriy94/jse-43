package com.tsconsulting.dsubbotin.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntityDTO implements Serializable {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Override
    public String toString() {
        return id;
    }

}