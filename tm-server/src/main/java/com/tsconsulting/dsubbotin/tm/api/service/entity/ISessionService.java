package com.tsconsulting.dsubbotin.tm.api.service.entity;

import com.tsconsulting.dsubbotin.tm.entity.Session;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionService extends IService<Session> {

    @NotNull
    Session open(
            @NotNull String login,
            @NotNull String password
    ) throws AbstractException;

    boolean close(@NotNull Session session);

    void validate(@Nullable Session session) throws AbstractException;

    void validate(@Nullable Session session, @NotNull Role role) throws AbstractException;

    @NotNull
    User getUser(@NotNull Session session) throws AbstractException;

    @NotNull
    String getUserId(@NotNull Session session);

}
