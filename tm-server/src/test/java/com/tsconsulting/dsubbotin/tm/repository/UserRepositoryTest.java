package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.repository.entity.UserRepository;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;

public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final EntityManager entityManager;

    public UserRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        userRepository = new UserRepository(entityManager);
        user = new User();
        user.setLogin(userLogin);
        @NotNull final String password = "userTest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(iteration, secret, password));
    }

    @Before
    public void initializeTest() {
        entityManager.getTransaction().begin();
        userRepository.create(user);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findByLogin() throws AbstractException {
        @Nullable final User foundUser = userRepository.findByLogin(userLogin);
        Assert.assertNotNull(foundUser);
        Assert.assertEquals(user.getId(), foundUser.getId());
        Assert.assertEquals(user.getLogin(), foundUser.getLogin());
        Assert.assertEquals(user.getPasswordHash(), foundUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), foundUser.getRole());
    }

    @Test
    public void remove() {
        entityManager.getTransaction().begin();
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        Assert.assertFalse(userRepository.existById(user.getId()));
    }

    @After
    public void finalizeTest() {
        entityManager.getTransaction().begin();
        if (userRepository.existById(user.getId())) userRepository.remove(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
