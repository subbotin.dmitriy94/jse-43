package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.entity.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.entity.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.entity.Project;
import com.tsconsulting.dsubbotin.tm.entity.Task;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.repository.entity.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.repository.entity.TaskRepository;
import com.tsconsulting.dsubbotin.tm.repository.entity.UserRepository;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final Task task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTask";

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String userId;

    @NotNull
    private final EntityManager entityManager;

    public TaskRepositoryTest() {
        user = new User();
        userId = user.getId();
        user.setLogin("guest");
        user.setPasswordHash(HashUtil.salt(3, "qwe", "guest"));
        task = new Task();
        taskId = task.getId();
        task.setUser(user);
        task.setName(taskName);
        task.setDescription(taskDescription);
        project = new Project();
        projectId = project.getId();
        project.setUser(user);
        project.setName("Project");
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskRepository(entityManager);
        projectRepository = new ProjectRepository(entityManager);
        userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.create(user);
        entityManager.getTransaction().commit();

    }

    @Before
    public void initializeTest() {
        entityManager.getTransaction().begin();
        taskRepository.create(task);
        projectRepository.create(project);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findTask() throws AbstractException {
        checkTask(taskRepository.findByName(userId, taskName));
        checkTask(taskRepository.findById(taskId));
        checkTask(taskRepository.findById(userId, taskId));
        checkTask(taskRepository.findByIndex(userId, 0));
        @NotNull final List<Task> tasks = taskRepository.findAll(userId);
        Assert.assertEquals(tasks.size(), 1);
    }

    private void checkTask(@Nullable final Task foundTask) {
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(task.getId(), foundTask.getId());
        Assert.assertEquals(task.getName(), foundTask.getName());
        Assert.assertEquals(task.getDescription(), foundTask.getDescription());
        Assert.assertEquals(task.getUser().getId(), foundTask.getUser().getId());
        Assert.assertEquals(task.getStartDate(), foundTask.getStartDate());
    }

    @Test
    public void remove() {
        Assert.assertNotNull(task);
        entityManager.getTransaction().begin();
        taskRepository.remove(task);
        entityManager.getTransaction().commit();
        Assert.assertFalse(taskRepository.existById(userId, taskId));
    }

    @Test
    public void findAllByProjectId() throws AbstractException {
        entityManager.getTransaction().begin();
        taskRepository.findById(userId, taskId).setProject(project);
        entityManager.getTransaction().commit();
        @Nullable final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
        Assert.assertEquals(taskList.size(), 1);
        @NotNull final Task tmpTask = taskList.get(0);
        Assert.assertEquals(task.getId(), tmpTask.getId());
        Assert.assertEquals(task.getName(), tmpTask.getName());
        Assert.assertEquals(task.getDescription(), tmpTask.getDescription());
        Assert.assertNotNull(task.getProject());
        Assert.assertNotNull(tmpTask.getProject());
        Assert.assertEquals(task.getProject().getId(), tmpTask.getProject().getId());
    }

    @Test
    public void removeAllTaskByProjectId() throws AbstractException {
        entityManager.getTransaction().begin();
        @NotNull final Task task1 = new Task();
        task1.setName("Task1");
        task1.setUser(user);
        taskRepository.create(task1);
        task1.setProject(project);
        taskRepository.findById(userId, taskId).setProject(project);
        entityManager.getTransaction().commit();
        @Nullable final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
        Assert.assertFalse(taskList.isEmpty());
        Assert.assertEquals(2, taskList.size());
        entityManager.getTransaction().begin();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(taskRepository.findAllByProjectId(userId, projectId).size(), 0);
    }

    @After
    public void finalizeTest() {
        entityManager.getTransaction().begin();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
