package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.IUserService;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.service.entity.UserService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public final class UserServiceTest {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final String userId;

    @NotNull
    final private String userLogin = "test";

    @Nullable
    final private String userPassword;

    final private int iteration;

    @NotNull
    final private String secret;

    public UserServiceTest() throws AbstractException {
        @NotNull IPropertyService propertyService = new PropertyService();
        iteration = propertyService.getPasswordIteration();
        secret = propertyService.getPasswordSecret();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final LogService logService = new LogService();
        userService = new UserService(connectionService, logService);
        userId = userService.create(userLogin, "test").getId();
        userPassword = HashUtil.salt(iteration, secret, "test");
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newUserLogin = "test1";
        @NotNull final String newUserPassword = "test1";
        @NotNull final User newUser = userService.create(newUserLogin, newUserPassword);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        Assert.assertEquals(HashUtil.salt(iteration, secret, newUserPassword), newUser.getPasswordHash());
        userService.removeById(newUser.getId());
    }

    @Test
    public void findByLogin() throws AbstractException {
        @NotNull final User tempUser = userService.findByLogin(userLogin);
        Assert.assertEquals(userLogin, tempUser.getLogin());
        Assert.assertEquals(userPassword, tempUser.getPasswordHash());
    }

    @Test
    public void removeByLogin() throws AbstractException {
        Assert.assertFalse(userService.findAll().isEmpty());
        final int size = userService.findAll().size();
        userService.removeByLogin(userLogin);
        Assert.assertEquals(size - 1, userService.findAll().size());
    }

    @Test
    public void setPassword() throws AbstractException {
        Assert.assertEquals(userPassword, userService.findById(userId).getPasswordHash());
        @NotNull final String newPassword = "newPassword";
        userService.setPassword(userId, newPassword);
        @NotNull final User user = userService.findById(userId);
        Assert.assertNotEquals(userPassword, user.getPasswordHash());
        Assert.assertEquals(HashUtil.salt(iteration, secret, newPassword), user.getPasswordHash());
    }

    @Test
    public void setRole() throws AbstractException {
        @NotNull final User user = userService.findById(userId);
        Assert.assertNotNull(user.getRole());
        Assert.assertEquals(user.getRole(), Role.USER);
        userService.setRole(userId, Role.ADMIN);
        Assert.assertEquals(userService.findById(userId).getRole(), Role.ADMIN);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newLastName = "newLastName";
        @NotNull final String newFirstName = "newFirstName";
        @NotNull final String newMiddleName = "newMiddleName";
        @NotNull final String newEmail = "email";
        @NotNull final User user = userService.findById(userId);
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        Assert.assertNull(user.getEmail());
        userService.updateById(userId, newLastName, newFirstName, newMiddleName, newEmail);
        @NotNull final User updUser = userService.findById(userId);
        Assert.assertEquals(newLastName, updUser.getLastName());
        Assert.assertEquals(newFirstName, updUser.getFirstName());
        Assert.assertEquals(newMiddleName, updUser.getMiddleName());
        Assert.assertEquals(newEmail, updUser.getEmail());
    }

    @Test
    public void isLogin() throws AbstractException {
        Assert.assertTrue(userService.isLogin(userLogin));
    }

    @Test
    public void lockUnlockByLogin() throws AbstractException {
        Assert.assertFalse(userService.findById(userId).isLocked());
        userService.lockByLogin(userLogin);
        Assert.assertTrue(userService.findById(userId).isLocked());
        userService.unlockByLogin(userLogin);
        Assert.assertFalse(userService.findById(userId).isLocked());
    }

    @After
    public void finalizeTest() throws AbstractException {
        if (userService.isLogin(userLogin)) userService.removeById(userId);
    }

}
