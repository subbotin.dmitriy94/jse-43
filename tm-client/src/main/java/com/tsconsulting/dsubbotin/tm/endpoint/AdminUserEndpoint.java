package com.tsconsulting.dsubbotin.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.3
 * 2021-07-29T16:37:10.234+03:00
 * Generated source version: 3.4.3
 */
@WebService(targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "AdminUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminUserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findAllUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findAllUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findAllUser/Fault/AbstractException")})
    @RequestWrapper(localName = "findAllUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.FindAllUser")
    @ResponseWrapper(localName = "findAllUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.FindAllUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.tsconsulting.dsubbotin.tm.endpoint.UserDTO> findAllUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/unlockByLoginUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/unlockByLoginUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/unlockByLoginUser/Fault/AbstractException")})
    @RequestWrapper(localName = "unlockByLoginUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.UnlockByLoginUser")
    @ResponseWrapper(localName = "unlockByLoginUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.UnlockByLoginUserResponse")
    public void unlockByLoginUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByIndexUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByIndexUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByIndexUser/Fault/AbstractException")})
    @RequestWrapper(localName = "removeByIndexUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.RemoveByIndexUser")
    @ResponseWrapper(localName = "removeByIndexUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.RemoveByIndexUserResponse")
    public void removeByIndexUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "index", targetNamespace = "")
                    int index
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/setRoleUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/setRoleUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/setRoleUser/Fault/AbstractException")})
    @RequestWrapper(localName = "setRoleUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.SetRoleUser")
    @ResponseWrapper(localName = "setRoleUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.SetRoleUserResponse")
    public void setRoleUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id,
            @WebParam(name = "role", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.Role role
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByIdUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByIdUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByIdUser/Fault/AbstractException")})
    @RequestWrapper(localName = "findByIdUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.FindByIdUser")
    @ResponseWrapper(localName = "findByIdUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.FindByIdUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsconsulting.dsubbotin.tm.endpoint.UserDTO findByIdUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByIdUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByIdUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByIdUser/Fault/AbstractException")})
    @RequestWrapper(localName = "removeByIdUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.RemoveByIdUser")
    @ResponseWrapper(localName = "removeByIdUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.RemoveByIdUserResponse")
    public void removeByIdUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByLoginUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByLoginUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/removeByLoginUser/Fault/AbstractException")})
    @RequestWrapper(localName = "removeByLoginUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.RemoveByLoginUser")
    @ResponseWrapper(localName = "removeByLoginUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.RemoveByLoginUserResponse")
    public void removeByLoginUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByLoginUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByLoginUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByLoginUser/Fault/AbstractException")})
    @RequestWrapper(localName = "findByLoginUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.FindByLoginUser")
    @ResponseWrapper(localName = "findByLoginUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.FindByLoginUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsconsulting.dsubbotin.tm.endpoint.UserDTO findByLoginUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/lockByLoginUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/lockByLoginUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/lockByLoginUser/Fault/AbstractException")})
    @RequestWrapper(localName = "lockByLoginUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.LockByLoginUser")
    @ResponseWrapper(localName = "lockByLoginUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.LockByLoginUserResponse")
    public void lockByLoginUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByIndexUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByIndexUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/findByIndexUser/Fault/AbstractException")})
    @RequestWrapper(localName = "findByIndexUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.FindByIndexUser")
    @ResponseWrapper(localName = "findByIndexUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.FindByIndexUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsconsulting.dsubbotin.tm.endpoint.UserDTO findByIndexUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "index", targetNamespace = "")
                    int index
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/clearUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/clearUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/clearUser/Fault/AbstractException")})
    @RequestWrapper(localName = "clearUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.ClearUser")
    @ResponseWrapper(localName = "clearUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.ClearUserResponse")
    public void clearUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session
    ) throws AbstractException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/createUserRequest", output = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/createUserResponse", fault = {@FaultAction(className = AbstractException_Exception.class, value = "http://endpoint.tm.dsubbotin.tsconsulting.com/AdminUserEndpoint/createUser/Fault/AbstractException")})
    @RequestWrapper(localName = "createUser", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.CreateUser")
    @ResponseWrapper(localName = "createUserResponse", targetNamespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", className = "com.tsconsulting.dsubbotin.tm.endpoint.CreateUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsconsulting.dsubbotin.tm.endpoint.UserDTO createUser(

            @WebParam(name = "session", targetNamespace = "")
                    com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password,
            @WebParam(name = "email", targetNamespace = "")
                    java.lang.String email
    ) throws AbstractException_Exception;
}
