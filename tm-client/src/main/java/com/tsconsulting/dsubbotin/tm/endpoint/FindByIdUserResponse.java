package com.tsconsulting.dsubbotin.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for findByIdUserResponse complex type.
 * <p>
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * <p>
 * &lt;pre&gt;
 * &amp;lt;complexType name="findByIdUserResponse"&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="return" type="{http://endpoint.tm.dsubbotin.tsconsulting.com/}userDTO" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findByIdUserResponse", propOrder = {
        "_return"
})
public class FindByIdUserResponse {

    @XmlElement(name = "return")
    protected UserDTO _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link UserDTO }
     */
    public UserDTO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link UserDTO }
     */
    public void setReturn(UserDTO value) {
        this._return = value;
    }

}
