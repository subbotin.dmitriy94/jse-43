package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.endpoint.Status;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class ProjectUpdateStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-update-status-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Update status project by index.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber();
        endpointLocator.getProjectEndpoint().findByIndexProject(session, index);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = EnumerationUtil.parseStatus(statusValue);
        endpointLocator.getProjectEndpoint().updateStatusByIndexProject(session, index, status);
        TerminalUtil.printMessage("[Updated project status]");
    }

}
