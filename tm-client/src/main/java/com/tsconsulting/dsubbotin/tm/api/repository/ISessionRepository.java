package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import org.jetbrains.annotations.Nullable;

public interface ISessionRepository {

    @Nullable
    SessionDTO getSession();

    void setSession(@Nullable final SessionDTO session);

}
