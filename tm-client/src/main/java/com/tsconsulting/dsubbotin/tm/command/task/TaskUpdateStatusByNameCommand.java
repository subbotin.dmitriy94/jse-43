package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.endpoint.Status;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class TaskUpdateStatusByNameCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-update-status-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Update status task by name.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().findByNameTask(session, name);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = EnumerationUtil.parseStatus(statusValue);
        endpointLocator.getTaskEndpoint().updateStatusByNameTask(session, name, status);
        TerminalUtil.printMessage("[Updated task status]");

    }

}
