package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ISessionService;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@RequiredArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionDTO getSession() {
        return sessionRepository.getSession();
    }

    public void setSession(@Nullable final SessionDTO session) {
        sessionRepository.setSession(session);
    }

}
