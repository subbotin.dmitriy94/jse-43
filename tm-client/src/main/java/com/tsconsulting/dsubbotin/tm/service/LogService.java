package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public final class LogService implements ILogService {

    @NotNull
    private final static String FILE_NAME = "logger.properties";

    @NotNull
    private final static String COMMANDS = "COMMANDS";

    @NotNull
    private final static String COMMANDS_FILE = "./commands.log";

    @NotNull
    private final static String ERRORS = "ERRORS";

    @NotNull
    private final static String ERRORS_FILE = "./errors.log";


    @NotNull
    private final static Handler CONSOLE_HANDLER = getConsoleHandler();

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger root = Logger.getLogger("");

    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);

    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
    }

    @NotNull
    private static Handler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            if (inputStream == null) {
                root.severe("Error! Cannot find file: " + FILE_NAME);
                return;
            }
            manager.readConfiguration(inputStream);
            inputStream.close();
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@NotNull final String message) {
        if (EmptyUtil.isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void debug(@NotNull final String message) {
        if (EmptyUtil.isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void command(@NotNull final String message) {
        if (EmptyUtil.isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void error(@NotNull final Exception e) {
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
