package com.tsconsulting.dsubbotin.tm.command.data.binary;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBinSaveCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return "data-save-bin";
    }

    @Override
    public @NotNull String description() {
        return "Save binary data.";
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().saveBinary(session);
        TerminalUtil.printMessage("Save to binary completed.");
    }

}
