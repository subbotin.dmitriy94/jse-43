package com.tsconsulting.dsubbotin.tm.command.data.jaxb;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataJsonLoadJaxbCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return "data-load-json-jaxb";
    }

    @Override
    public @NotNull String description() {
        return "Data loaded from JSON using JAXB.";
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().dataLoadJsonJaxb(session);
        TerminalUtil.printMessage("Load completed.");
        TerminalUtil.printMessage("Logged out. Please log in.");
    }

}
