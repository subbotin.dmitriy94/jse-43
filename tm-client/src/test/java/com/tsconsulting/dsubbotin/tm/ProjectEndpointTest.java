package com.tsconsulting.dsubbotin.tm;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import com.tsconsulting.dsubbotin.tm.endpoint.*;
import com.tsconsulting.dsubbotin.tm.marker.SoapCategory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class ProjectEndpointTest {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String projectName = "projectName";

    @NotNull
    private final String projectDescription = "projectDescription";

    @Nullable
    private String projectId;

    @NotNull
    private ProjectDTO project;

    @NotNull
    private SessionDTO session;

    public ProjectEndpointTest() {
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Before
    public void initializeTest() throws AbstractException_Exception {
        session = sessionEndpoint.openSession("user", "user");
        project = projectEndpoint.createProject(session, projectName, projectDescription);
        projectId = project.getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllProject() throws AbstractException_Exception {
        Assert.assertEquals(1, projectEndpoint.findAllProject(session, null).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findProject() throws AbstractException_Exception {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectEndpoint.findByIdProject(session, projectId));
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectEndpoint.findByNameProject(session, projectName));
        Assert.assertNotNull(projectEndpoint.findByIndexProject(session, 1));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdProject() {
        Assert.assertNotNull(projectId);
        removeProject(projectId);
        boolean found;
        try {
            projectEndpoint.findAllProject(session, null);
            found = true;
        } catch (ServerSOAPFaultException | AbstractException_Exception e) {
            found = !e.getMessage().contains("Project not found!");
        }
        Assert.assertFalse(found);
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void createProject() throws AbstractException_Exception {
        @NotNull final String tempProjectId =
                projectEndpoint.createProject(session, "test", "test").getId();
        Assert.assertNotNull(tempProjectId);
        Assert.assertNotNull(projectEndpoint.findByIdProject(session, tempProjectId));
        removeProject(tempProjectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIdProject() throws AbstractException_Exception {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectName";
        projectEndpoint.updateByIdProject(session, projectId, newProjectName, newProjectDescription);
        @NotNull final ProjectDTO updatedProject = projectEndpoint.findByIdProject(session, projectId);
        Assert.assertEquals(projectId, updatedProject.getId());
        Assert.assertNotEquals(projectName, updatedProject.getName());
        Assert.assertNotEquals(projectDescription, updatedProject.getDescription());
        Assert.assertEquals(newProjectName, updatedProject.getName());
        Assert.assertEquals(newProjectDescription, updatedProject.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void startByIdProject() throws AbstractException_Exception {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.startByIdProject(session, projectId);
        Assert.assertEquals(projectEndpoint.findByIdProject(session, projectId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishByIdProject() throws AbstractException_Exception {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.finishByIdProject(session, projectId);
        Assert.assertEquals(projectEndpoint.findByIdProject(session, projectId).getStatus(), Status.COMPLETED);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateStatusByIdProject() throws AbstractException_Exception {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.updateStatusByIdProject(session, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(projectEndpoint.findByIdProject(session, projectId).getStatus(), Status.IN_PROGRESS);
        projectEndpoint.updateStatusByIdProject(session, projectId, Status.COMPLETED);
        Assert.assertEquals(projectEndpoint.findByIdProject(session, projectId).getStatus(), Status.COMPLETED);
    }

    @After
    public void finalizeTest() throws AbstractException_Exception {
        if (projectId != null) removeProject(projectId);
        sessionEndpoint.closeSession(session);
    }

    @SneakyThrows
    private void removeProject(@NotNull final String id) {
        new ProjectTaskEndpointService().getProjectTaskEndpointPort().removeProjectById(session, id);
    }

}
